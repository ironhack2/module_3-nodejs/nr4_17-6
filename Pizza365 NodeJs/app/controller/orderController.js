// import order model
const orderModel = require('../model/orderModel');
const userModel = require('../model/userModel');
const voucherModel = require('../model/voucherModel');
const drinkModel = require('../model/drinkModel');

// import mongoose
const mongoose = require('mongoose');

const createOrder = (request, response) => {
    //b1: chuẩn bị dữ liệu
    let userId = request.params.userId;

    let requestBody = request.body;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User ID is invalid"
        })
    }

    if (!requestBody.orderCode) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "orderCode is invalid"
        })
    }

    if (!requestBody.pizzaSize) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaSize is invalid"
        })
    }

    if (!requestBody.pizzaType) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "pizzaType is invalid"
        })
    }

    if (!requestBody.status) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "status is invalid"
        })
    }

    // b3: thao tác với cơ sở dữ liệu
    let createOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: requestBody.orderCode,
        pizzaSize: requestBody.pizzaSize,
        pizzaType: requestBody.pizzaType,
        drink: requestBody.drink,
        voucher: requestBody.voucher,
        status: requestBody.status,
    }

    orderModel.create(createOrder, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            userModel.findByIdAndUpdate(userId, {
                    $push: { orders: data._id }
                },
                (err, updateUser) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: "Create Order Success",
                            data: data
                        })
                    }
                })
        }
    })
}

const getAllOrder = (request, response) => {
    // bước 1: thu thập dữ liệu
    let userId = request.params.userId;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    userModel.findById(userId)
        .populate('orders')
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal Error Sever",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Get All order Success",
                    data: data.orders
                })
            }
        })
}

const getOrderById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let orderId = request.params.orderId;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Order Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            return response.status(200).json({
                status: "Get Order By Id Success",
                data: data
            })
        }
    })
}

const updateOrderById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let orderBody = request.body;

    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Order Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu

    let save_drink = "";

    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            const save_drink = data.drink.drinkId;
            const save_voucher = data.voucher.voucherId;

            let updateOrder = {
                pizzaSize: orderBody.pizzaSize,
                pizzaType: orderBody.pizzaType,
                drink: { drinkId: orderBody.drinkId ? orderBody.drinkId : save_drink },
                voucher: { voucherId: orderBody.voucherId ? orderBody.voucherId : save_voucher },
                status: orderBody.status,
            }


            orderModel.findByIdAndUpdate(orderId, updateOrder, (error, data) => {
                if (error) {
                    return response.status(500).json({
                        status: "Error 500: Internal sever error",
                        message: error.message
                    })
                } else {
                    return response.status(200).json({
                        status: "Update Order By Id Success",
                        data: data
                    })
                }
            })
        }
    })
}

const deleteOrderById = (request, response) => {
    // bước 1: thu thập dữ liệu
    let orderId = request.params.orderId;
    let userId = request.params.userId;
    // bước 2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Order Id is invalid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User Id is invalid"
        })
    }
    // bước 3: thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: err.message
            })
        } else {
            userModel.findByIdAndUpdate(userId, {
                    $pull: { orders: orderId }
                },
                (err, updateUser) => {
                    if (err) {
                        return response.status(500).json({
                            status: "Error 500: Internal sever error",
                            message: err.message
                        })
                    } else {
                        return response.status(204).json({
                            status: "Delete Order success"
                        })
                    }
                }
            )
        }
    })
}

const createNewOrder = (request, response) => {
    // bước 1: thu thập dữ liệu
    var requestBody = request.body
    var email = requestBody.email
        // bước 2: kiểm tra dữ liệu
    if (email == "") {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: 'Email is not require'
        })
    } else {
        userModel.findOne({ email }, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal Sever Error",
                    message: error.message
                })
            } else {
                if (!data) {
                    let createOrder = {
                        _id: mongoose.Types.ObjectId(),
                        orderCode: Math.random().toString(36).substring(1, 8),
                        pizzaSize: requestBody.kichCo,
                        duongKinh: requestBody.duongKinh,
                        suon: requestBody.suon,
                        salad: requestBody.salad,
                        pizzaType: requestBody.loaiPizza,
                        drink: requestBody.idLoaiNuocUong,
                        thanhTien: requestBody.thanhTien,
                        voucher: requestBody.idVourcher,
                        status: requestBody.loiNhan,
                        soLuongNuoc: requestBody.soLuongNuoc
                    }
                    orderModel.create(createOrder, (error, data) => {
                        if (error) {
                            return response.status(500).json({
                                status: "Error 500: Internal Sever Error",
                                message: error.message
                            })
                        } else {
                            let discount = 0
                            let drink = ""
                            voucherModel.findById(requestBody.idVourcher, (error, data) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal Sever Error",
                                        message: error.message
                                    })
                                } else {
                                    discount = data.phanTramGiamGia
                                }
                            })
                            drinkModel.findById(requestBody.idLoaiNuocUong, (error, data) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal Sever Error",
                                        message: error.message
                                    })
                                } else {
                                    drink = data.tenNuocUong
                                }
                            })
                            let NewUser = {
                                _id: mongoose.Types.ObjectId(),
                                fullName: requestBody.hoTen,
                                email: requestBody.email,
                                address: requestBody.diaChi,
                                phone: requestBody.soDienThoai,
                                orders: data._id
                            }
                            userModel.create(NewUser, (error, data2) => {
                                if (error) {
                                    response.status(500).json({
                                        status: "Error 500: Internal Sever Error",
                                        message: error.message
                                    })
                                } else {
                                    return response.status(201).json({
                                        status: 'Create Order Success',
                                        data: {
                                            id: data._id,
                                            orderId: data.orderCode,
                                            kichCo: data.pizzaSize,
                                            duongKinh: data.duongKinh,
                                            suon: data.suon,
                                            salad: data.salad,
                                            loaiPizza: data.pizzaType,
                                            idVourcher: data.voucher,
                                            thanhTien: data.thanhTien,
                                            giamGia: discount,
                                            idLoaiNuocUong: drink,
                                            soLuongNuoc: data.soLuongNuoc,
                                            hoTen: data2.fullName,
                                            email: data2.email,
                                            soDienThoai: data2.phone,
                                            diaChi: data2.address,
                                            loiNhan: data.status,
                                            trangThai: "open",
                                            ngayTao: data.ngayTao,
                                            ngayCapNhat: data.ngayCapNhat,
                                        }
                                    })
                                }
                            })
                        }
                    })
                } else {
                    let createOrder = {
                        _id: mongoose.Types.ObjectId(),
                        orderCode: Math.random().toString(36).substring(1, 8),
                        pizzaSize: requestBody.kichCo,
                        duongKinh: requestBody.duongKinh,
                        suon: requestBody.suon,
                        salad: requestBody.salad,
                        pizzaType: requestBody.loaiPizza,
                        drink: requestBody.idLoaiNuocUong,
                        thanhTien: requestBody.thanhTien,
                        voucher: requestBody.idVourcher,
                        status: requestBody.loiNhan,
                        soLuongNuoc: requestBody.soLuongNuoc
                    }
                    orderModel.create(createOrder, (error, data1) => {
                        if (error) {
                            return response.status(500).json({
                                status: "Error 500: Internal Sever Error",
                                message: error.message
                            })
                        } else {
                            let discount = 0
                            let drink = ""
                            voucherModel.findById(requestBody.idVourcher, (error, data) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal Sever Error",
                                        message: error.message
                                    })
                                } else {
                                    discount = data.phanTramGiamGia
                                }
                            })
                            drinkModel.findById(requestBody.idLoaiNuocUong, (error, data) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Error 500: Internal Sever Error",
                                        message: error.message
                                    })
                                } else {
                                    drink = data.tenNuocUong
                                }
                            })
                            userModel.findByIdAndUpdate(data._id, { $push: { orders: data1._id } },
                                (error, data) => {
                                    return response.status(201).json({
                                        status: "Create Order Succes by Email",
                                        data: {
                                            id: data1._id,
                                            orderId: data1.orderCode,
                                            kichCo: data1.pizzaSize,
                                            duongKinh: data1.duongKinh,
                                            suon: data1.suon,
                                            salad: data1.salad,
                                            loaiPizza: data1.pizzaType,
                                            idVourcher: data1.voucher,
                                            thanhTien: data1.thanhTien,
                                            giamGia: discount,
                                            idLoaiNuocUong: drink,
                                            soLuongNuoc: data.soLuongNuoc,
                                            hoTen: data.fullName,
                                            email: data.email,
                                            soDienThoai: data.phone,
                                            diaChi: data.address,
                                            loiNhan: data1.status,
                                            trangThai: "open",
                                            ngayTao: data1.ngayTao,
                                            ngayCapNhat: data1.ngayCapNhat,
                                        }
                                    })
                                })
                        }
                    })
                }
            }
        })
    }

    // bước 3: thao tác với cơ sở dữ liệu
}

// export thành 1 module
module.exports = {
    createOrder: createOrder,
    getAllOrder: getAllOrder,
    getOrderById: getOrderById,
    updateOrderById: updateOrderById,
    deleteOrderById: deleteOrderById,
    createNewOrder: createNewOrder
}