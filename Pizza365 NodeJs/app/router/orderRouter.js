// khởi tạo bộ thư viện
const express = require('express');

const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById, createNewOrder } = require('../controller/orderController');

//khởi tạo router
const orderRouter = express.Router();

orderRouter.get("/users/:userid/orders", getAllOrder)

orderRouter.post("/users/:userid/orders", createOrder)

orderRouter.get("/orders/:orderId", getOrderById)

orderRouter.put("/orders/:orderId", updateOrderById)

orderRouter.delete("/users/:userid/orders/:orderId", deleteOrderById)

orderRouter.post("/devcamp-pizza365/orders", createNewOrder)


// export dữ liệu thành 1 module
module.exports = orderRouter;