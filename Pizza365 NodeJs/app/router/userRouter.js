// khởi tạo bộ thư viện
const express = require('express');
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllLimitUser, getAllSkipUser, getAllSortUser, getAllSkipLimitUser, getAllSortSkipLimitUser } = require('../controller/userController');

//khởi tạo router
const userRouter = express.Router();

userRouter.get("/user", getAllUser)

userRouter.get("/limit-users", getAllLimitUser)

userRouter.get("/skip-users", getAllSkipUser)

userRouter.get("/skip-limit-users", getAllSkipLimitUser)

userRouter.get("/sort-skip-limit-users", getAllSortSkipLimitUser)

userRouter.get("/sort-users", getAllSortUser)

userRouter.post("/user", createUser)

userRouter.get("/user/:userId", getUserById)

userRouter.put("/user/:userId", updateUserById)

userRouter.delete("/user/:userId", deleteUserById)

// export dữ liệu thành 1 module
module.exports = userRouter;