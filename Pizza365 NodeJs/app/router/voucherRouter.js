// khởi tạo bộ thư viện
const express = require('express');

const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controller/voucherController')

//khởi tạo router
const voucherRouter = express.Router();

voucherRouter.get("/voucher", getAllVoucher)

voucherRouter.post("/voucher", createVoucher)

voucherRouter.get("/voucher/:voucherId", getVoucherById)

voucherRouter.put("/voucher/:voucherId", updateVoucherById)

voucherRouter.delete("/voucher/:voucherId", deleteVoucherById)

// export dữ liệu thành 1 module
module.exports = voucherRouter;