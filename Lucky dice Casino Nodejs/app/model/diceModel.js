// Khai báo thư viện mongoose
const mongoose = require('mongoose');

//  Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//  Khởi tạo Schema với các thuộc tính được yêu cầu
const diceSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
    },
    username: {
        type: String,
        required: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
});

//  EXPORT
module.exports = mongoose.model("dice", diceSchema);