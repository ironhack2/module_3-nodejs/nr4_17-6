// Khai báo thư viện mongoose
const mongoose = require('mongoose');

// Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo Schema với các thuộc tính được yêu cầu
const prizeHistorySchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    user: [{
        type: mongoose.Types.ObjectId,
        ref: "user",
        // required: true
    }],
    prize: [{
        type: mongoose.Types.ObjectId,
        ref: "voucher",
        // required: true
    }],
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt: {
        type: Date,
        default: Date.now()
    }
})

// EXPORT
module.exports = mongoose.model("prizeHistory", prizeHistorySchema);