//Khai báo thư viện mongoose
const mongoose = require('mongoose');

//Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Khai báo thuộc tính Schema được yêu cầu
const voucherHistorySchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    user: [{
        type: mongoose.Types.ObjectId,
        ref: "user",
        // required: true
    }],
    voucher: [{
        type: mongoose.Types.ObjectId,
        ref: "voucher",
        // required: true
    }],
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt: {
        type: Date,
        default: Date.now()
    }
})

//EXPORT
module.exports = mongoose.model("voucherHistory", voucherHistorySchema);