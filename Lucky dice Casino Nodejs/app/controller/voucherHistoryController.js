// Khai báo mongoose
const mongoose = require('mongoose');

// Import Model
const voucherHistoryModel = require('../model/voucherHistoryModel');
const userModel = require('../model/userModel');

// CREATE VOUCHER HISTORY
const createVoucherHistory = (request, response) => {

    //B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User is required"
        })
    }

    if (!bodyRequest.voucher) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher is required"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        voucher: bodyRequest.voucher
    }
    voucherHistoryModel.create(createVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create Voucher History success",
                data: data
            })
        }
    })
}


//GET ALL VOUCHER HISTORY
const getAllVoucherHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let user = request.query.user;

    let condition = {};

    if (user) {
        condition.user = user;
    }

    //condition.title = courseName;
    //B2: validate dữ liệu
    // B3: thao tắc với cơ sở dữ liệu
    voucherHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: get all voucher history successfully",
                data: data
            })
        }
    })

}


//GET A VOUCHER HISTORY
const getVoucherHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let voucherHistoryId = request.params.voucherHistoryId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher History Id is not a valid"
        })
    }

    // B3: thao tắc với cơ sở dữ liệu
    voucherHistoryModel.findById(voucherHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success Get Voucher History By ID successfully",
                data: data
            })
        }
    })
}


// UPDATE VOUCHER HISTORY
const updateVoucherHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let voucherHistoryId = request.params.voucherHistoryId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher History Id is not a valid"
        })
    }

    // B3: thao tắc với cơ sở dữ liệu
    let updateVoucherHistory = {
        user: bodyRequest.user,
        voucher: bodyRequest.voucher
    }
    voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, updateVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success Update Voucher History By ID successfully",
                data: data
            })
        }
    })
}


// DELETE VOUCHER HISTORY
const deleteVoucherHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let voucherHistoryId = request.params.voucherHistoryId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Voucher History Id is not a valid"
        })
    }

    // B3: thao tắc với cơ sở dữ liệu
    voucherHistoryModel.findByIdAndDelete(voucherHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: delete voucher history successfully"
            })
        }
    })
}

// LẤY VOUCHER  HISTORY BY USERNAME
const getVoucherHistoryByUsername = (request, response) => {
    //B1: thu thập dữ liệu
    let username = request.query.username;

    if (!username) {
        voucherHistoryModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindUser.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All Voucher History success",
                    data: data,
                })
            }
        })

    } else {
        userModel.findOne({ username: username }, (errorFindUser, userExist) => {
            if (errorFindUser) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindUser.message
                })
            } else {
                if (!userExist) {
                    response.status(400).json({
                        status: "Error 400: Username không tồn tại",
                        data: []
                    })
                } else {
                    userModel.find({ username: username }, (error, findUsername) => {
                        // for (let bI = 0; bI < findUsername.length; bI++) {

                        //B3 Thao tác với cơ sở dữ liệu
                        let condition = {};
                        condition.user = findUsername[0]._id;

                        voucherHistoryModel.find(condition, (error, data) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Username không tồn tại",
                                        data: []
                                    })
                                } else {
                                    return response.status(200).json({
                                        status: "Success: Get Voucher History By Username success",
                                        data: data,
                                    })
                                }
                            })
                            // }
                    })
                }

            }
        })
    }
}


// EXPORT
module.exports = { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistory, deleteVoucherHistory, getVoucherHistoryByUsername }