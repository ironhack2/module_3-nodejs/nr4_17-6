// Khai báo mongoose
const mongoose = require('mongoose');

// IMPORT prize model  vào controller
const prizeModel = require('../model/prizeModel');

// CREATE A PRIZE
const createPrize = (request, response) => {

    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: Điều kiện
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize is required"
        })
    }

    if (!bodyRequest.description) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "prize is required"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createPrize = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        description: bodyRequest.description
    }
    prizeModel.create(createPrize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create prize success",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ PRIZE
const getAllPrize = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}

// LẤY PRIZE THEO ID
const getPrizeById = (request, response) => {
    //B1: thu thập dữ liệu
    let prizeId = request.params.prizeId;

    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Prize ID is not valid"
        })
    }

    // B3: Thao tắc với cơ sở dữ liệu
    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Prize by id success",
                data: data
            })
        }
    })
}


//UPDATE A PRIZE
const updatePrizeById = (request, response) => {
    // B1: Thu thập dữ liệu
    let prizeId = request.params.prizeId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Prize ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let updatePrize = {
        name: bodyRequest.name,
        description: bodyRequest.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Prize success",
                data: data
            })
        }
    })
}


// DELETE A PRIZE
const deletePrizeById = (request, response) => {
    // B1: thu thập dữ liệu
    let prizeId = request.params.prizeId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Prize Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Prize success",
            })
        }
    })
}

// EXPORT
module.exports = { createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById }