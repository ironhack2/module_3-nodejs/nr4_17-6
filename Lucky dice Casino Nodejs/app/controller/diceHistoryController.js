// IMPORT dice history model  vào controller
const diceHistoryModel = require('../model/diceHistoryModel');
const userModel = require('../model/userModel');


//  Khai báo mongoose
const mongoose = require('mongoose');


// CREATE A DICE HISTORY
const createHistory = (request, response) => {

    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: điều kiện dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user is required"
        })
    }

    // if (!(Number.isInteger(bodyRequest.dice) && bodyRequest.dice > 0)) {
    //     return response.status(400).json({
    //         status: "Error 400: Bad Request",
    //         message: "dice is not valid"
    //     })
    // }

    //B3: Thao tác với cơ sở dữ liệu
    let createDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        dice: Math.floor(Math.random() * 6) + 1
    }

    diceHistoryModel.create(createDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create dice history success",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ DICE HISTORY
const getAllDiceHistory = (request, response) => {
    //B1: thu thập dữ liệu
    let user = request.query.user;

    let condition = {};

    if (user) {
        condition.user = user
    }


    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    diceHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}


// LẤY DICE HISTORY THEO ID
const getDiceHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    diceHistoryModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Dice History by id success",
                data: data
            })
        }
    })
}


//UPDATE A USER
const updateDiceHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Dice History ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateDiceHistory = {
        user: bodyRequest.user,
        dice: bodyRequest.dice
    }
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Dice History success",
                data: data
            })
        }
    })
}


// DELETE A USER
const deleteDiceHistoryById = (request, response) => {
    //B1: thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "Drink Id is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    diceHistoryModel.findOneAndDelete(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Dice History success"
            })
        }
    })
}


// LẤY DICE HISTORY BY USERNAME
const getDiceHistoryByUsername = (request, response) => {
    //B1: thu thập dữ liệu
    let username = request.query.username;

    var condition = {};

    if (!username) {
        diceHistoryModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindUser.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All Dice History success",
                    data: data,
                })
            }
        })

    } else {
        userModel.findOne({ username: username }, (errorFindUser, userExist) => {
            if (errorFindUser) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: errorFindUser.message
                })

            } else {
                if (!userExist) {
                    response.status(400).json({
                        status: "Error 400: Username không tồn tại",
                        data: []
                    })
                } else {
                    if (username) {
                        condition.username = username;
                    }

                    userModel.find(condition, (error, findUsername) => {
                        // for (let bI = 0; bI < findUsername.length; bI++) {
                        if (error) {
                            response.status(400).json({
                                status: "Error 400: Username không tồn tại",
                                data: []
                            })
                        } else {
                            //B3 Thao tác với cơ sở dữ liệu
                            condition.user = findUsername[0]._id;

                            diceHistoryModel.find(condition, (error, data) => {
                                if (error) {
                                    return response.status(500).json({
                                        status: "Username không tồn tại",
                                        data: []
                                    })
                                } else {
                                    return response.status(200).json({
                                        status: "Success: Get Dice History By Username success",
                                        data: data,
                                    })
                                }
                            })
                        }
                    })
                }
            }

        })
    }
}


module.exports = { createHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById, getDiceHistoryByUsername };