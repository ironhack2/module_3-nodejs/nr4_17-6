// Khai báo thư viện express
const express = require('express');

// Import Middleware
const { diceHistoryMiddleware } = require('../middleware/diceHistoryMiddleware');


//Import  Controller
const { createHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById, getDiceHistoryByUsername } = require('../controller/diceHistoryController');

// Tạo router
const diceHistoryRouter = express.Router();

// Sử dụng middleware
diceHistoryRouter.use(diceHistoryMiddleware);


// KHAI BÁO API

//CREATE A DICE HISTORY
diceHistoryRouter.post('/dice-histories', createHistory);


//GET ALL DICE HISTORY
diceHistoryRouter.get('/dice-histories', getAllDiceHistory);


//GET A DICE HISTORY
diceHistoryRouter.get('/dice-histories/:diceHistoryId', getDiceHistoryById);


//UPDATE A DICE HISTORY
diceHistoryRouter.put('/dice-histories/:diceHistoryId', updateDiceHistoryById);


//DELETE A DICE HISTORY
diceHistoryRouter.delete('/dice-histories/:diceHistoryId', deleteDiceHistoryById);


//GET DICE HISTORY BY USERNAME
diceHistoryRouter.get('/devcamp-lucky-dice/dice-history', getDiceHistoryByUsername);


// EXPORT ROUTER
module.exports = diceHistoryRouter;


// {
//     "user": "22a95a300c3967918bae1d88",
//     "dice": 5
// }