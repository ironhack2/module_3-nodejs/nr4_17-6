// Khai báo thư viện express
const express = require('express');

// Import Middleware
const { diceMiddleware } = require('../middleware/diceMiddleware');


//Import  Controller
const { diceHandler } = require('../controller/diceController');

// Tạo router
const diceRouter = express.Router();

// Sử dụng middleware
diceRouter.use(diceMiddleware);


// KHAI BÁO API

diceRouter.post('/devcamp-lucky-dice/dice', diceHandler)



// EXPORT ROUTER
module.exports = diceRouter;